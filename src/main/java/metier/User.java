package metier;

public class User {
    private String MpsHach;
    private String identifiant;

    public User(String mpsHach, String identifiant) {
        MpsHach = mpsHach;
        this.identifiant = identifiant;
    }

    public String getMpsHach() {
        return MpsHach;
    }

    public void setMpsHach(String mpsHach) {
        MpsHach = mpsHach;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    @Override
    public String toString() {
        return "User{" +
                "MpsHach='" + MpsHach + '\'' +
                ", identifiant='" + identifiant + '\'' +
                '}';
    }
}
