package metier;

public class Login {
    private String loginIdentifiant;
    private String loginMdp;

    public Login(String loginIdentifiant, String loginMdp) {
        this.loginIdentifiant = loginIdentifiant;
        this.loginMdp = loginMdp;
    }

    public String getLoginIdentifiant() {
        return loginIdentifiant;
    }

    public void setLoginIdentifiant(String loginIdentifiant) {
        this.loginIdentifiant = loginIdentifiant;
    }

    public String getLoginMdp() {
        return loginMdp;
    }

    public void setLoginMdp(String loginMdp) {
        this.loginMdp = loginMdp;
    }

    @Override
    public String toString() {
        return "Login{" +
                "loginIdentifiant='" + loginIdentifiant + '\'' +
                ", loginMdp='" + loginMdp + '\'' +
                '}';
    }
}
