package rest;


import hibernateDAO.GenericDAO;
import hibernateMetier.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/Gites")

public class GiteRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGiteList() {
        GenericDAO<GiteEntity,Integer> giteDAO = new GenericDAO(GiteEntity.class);
        ArrayList<GiteEntity> gitesList = (ArrayList<GiteEntity>) giteDAO.findAll();
        if (gitesList != null)
            return Response.ok(gitesList).build();
        else
            return Response.status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
            @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")

        public Response getGiteById (@PathParam("id") Integer id){
        GenericDAO articleDAO = new GenericDAO(GiteEntity.class);
          GiteEntity giteSearch = (GiteEntity) articleDAO.find(id);
          if (giteSearch!=null)
              return Response.ok(giteSearch).build();
          else return Response.status(Response.Status.NO_CONTENT).build();

        }
}
