package hibernateDAO;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.util.List;


public class GenericDAO<T, PK extends Serializable>
        implements IGenericDAO<T, PK> {

    private Session session;
    private Transaction tx;

    final private Class type;


    public GenericDAO(Class type) {
        this.type = type;
    }

    @Override
    public void saveOrUpdate(T t) {
        try {
            startOperation();
            session.saveOrUpdate(t);
            tx.commit();
        } catch (HibernateException e) {
            handleException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(T t) {
        try {
            startOperation();
            session.delete(t);
            tx.commit();
        } catch (HibernateException e) {
            handleException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public T find(PK id) {
        T obj = null;
        try {
            startOperation();
            obj = (T) session.get(type.getName(), id);
            tx.commit();
        } catch (HibernateException e) {
            handleException(e);
        } finally {
            session.close();
        }
        return obj;
    }

    @Override
    public List findAll() {
        List objects = null;
        try {
            System.out.println("---------------------ok----------");
            startOperation();
            System.out.println("---------------------ok----------");
            Query query = session.createQuery("from " + type.getName());
            objects = query.list();
            System.out.println(objects);
            tx.commit();
        } catch (HibernateException e) {
            handleException(e);
        } finally {
            session.close();
        }
        return objects;
    }

    public void handleException(HibernateException e) {
        System.err.println(e.getMessage());
    }

    public void startOperation() throws HibernateException {
        session = HibernateUtil.getSessionFactory().openSession();
        System.out.println(session + "ICI");
        tx = session.beginTransaction();
    }

}
