package hibernateDAO;

import java.io.Serializable;
import java.util.List;

public interface IGenericDAO <T, PK extends Serializable>  {


        /**
         * Retrieve an object that was previously persisted to the database using the indicated id as primary key
         */
        T find(PK id);

        /**
         * Save changes made to a persistent object.
         */
        void saveOrUpdate(T transientObject);

        /**
         * Remove an object from persistent storage in the database
         */
        void delete(T persistentObject);

        /**
         * Get all objects from the database
         */
        List findAll();

}
