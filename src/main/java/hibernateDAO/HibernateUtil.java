package hibernateDAO;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    public static final ThreadLocal session = new ThreadLocal();
    private static SessionFactory sessionFactory;

    static
    {
        if (sessionFactory == null)
        {
            try
            { System.out.println("SESSION : " + "merde");

                StandardServiceRegistry standardServiceRegistry = new StandardServiceRegistryBuilder().configure("/hibernate.cfg.xml").build();

                Metadata metadata = new MetadataSources(standardServiceRegistry)
                        .getMetadataBuilder()
                        .applyImplicitNamingStrategy(ImplicitNamingStrategyJpaCompliantImpl.INSTANCE)
                        .build();

                sessionFactory = metadata.getSessionFactoryBuilder().build();

//                sessionFactory = new Configuration().configure("/hibernate.cfg.xml").buildSessionFactory();
                System.out.println("SESSION : " + sessionFactory +"ICI----------------------------------");
            }
            catch (Throwable ex)
            {
                System.out.println("Initial SessionFactory creation failed : " + ex);
                throw new ExceptionInInitializerError(ex);
            }
        }
    }
    public static SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }
/*    private static final SessionFactory sessionFactory;

    static {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            sessionFactory = new Configuration().configure("/hibernate.cfg.xml").buildSessionFactory();
            System.out.println(sessionFactory +"   h");
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }*/
/*private static final SessionFactory sessionFactory;

    static {
        try {
            // Crée la SessionFactory
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (HibernateException ex) {
            throw new RuntimeException("Problème de configuration : " + ex.getMessage(), ex);
        }
    }

    public static final ThreadLocal session = new ThreadLocal();

    public static Session currentSession() throws HibernateException {
        Session s = (Session) session.get();
        // Ouvre une nouvelle Session, si ce Thread n'en a aucune
        if (s == null) {
            s = sessionFactory.openSession();
            session.set(s);
        }
        return s;
    }

    public static void closeSession() throws HibernateException {
        Session s = (Session) session.get();
        session.set(null);
        if (s != null)
            s.close();
    }*/
}
