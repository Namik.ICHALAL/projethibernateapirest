package DAO;

import metier.Personne;
import metier.TypePeriode;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class TypePeriodeDAO extends DAO<TypePeriode>{
    public TypePeriodeDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public TypePeriode getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<TypePeriode> getAll() {
        ResultSet rs;
        ArrayList<TypePeriode> liste = new ArrayList<TypePeriode>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  id_type_periode,libelle_periode" +
                    " from type_periode order by libelle_periode";
           TypePeriode typePeriode;
            rs = stmt.executeQuery(strCmd);
            while (rs.next())
            {;
                typePeriode= new TypePeriode(rs.getInt(1), rs.getString(2));
                liste.add(typePeriode );
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données type de période impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(TypePeriode objet) {
        return false;
    }

    @Override
    public boolean update(TypePeriode objet) {
        return false;
    }

    @Override
    public boolean delete(TypePeriode objet) {
        return false;
    }
}
