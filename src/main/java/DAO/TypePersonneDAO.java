package DAO;

import metier.Telephone;
import metier.TypePersonne;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class TypePersonneDAO extends DAO<TypePersonne> {
    public TypePersonneDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public TypePersonne getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<TypePersonne> getAll() {
        ResultSet rs;
        ArrayList<TypePersonne> liste = new ArrayList<TypePersonne>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  id_type,nom_type" +
                    " from type_personne order by nom_type ";
            TypePersonne typePersonne;
            rs = stmt.executeQuery(strCmd);
            while (rs.next())
            {;
                typePersonne= new TypePersonne(rs.getInt(1), rs.getString(2));
                liste.add(typePersonne );
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données ville impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(TypePersonne objet) {
        return false;
    }

    @Override
    public boolean update(TypePersonne objet) {
        return false;
    }

    @Override
    public boolean delete(TypePersonne objet) {
        return false;
    }
}
