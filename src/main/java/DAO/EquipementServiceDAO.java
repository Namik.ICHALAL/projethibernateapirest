package DAO;

import metier.Departement;
import metier.EquipementService;
import metier.Ville;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class EquipementServiceDAO extends DAO<EquipementService> {
    public EquipementServiceDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public EquipementService getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<EquipementService> getAll() {
        ResultSet rs;
        ArrayList<EquipementService> liste = new ArrayList<EquipementService>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  id_service,nom_service from equipement_et_service order by nom_service";

            rs = stmt.executeQuery(strCmd);

   EquipementService equipementService;

            while (rs.next())
            {
                equipementService = new EquipementService(rs.getInt(1),rs.getString(2));
                liste.add(equipementService);
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données ville impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(EquipementService objet) {
        return false;
    }

    @Override
    public boolean update(EquipementService objet) {
        return false;
    }

    @Override
    public boolean delete(EquipementService objet) {
        return false;
    }
}
