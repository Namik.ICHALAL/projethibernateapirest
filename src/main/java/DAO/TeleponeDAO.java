package DAO;

import metier.Personne;
import metier.Telephone;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class TeleponeDAO extends DAO<Telephone>{
    public TeleponeDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Telephone getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Telephone> getAll() {

        ResultSet rs;
        ArrayList<Telephone> liste = new ArrayList<Telephone>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  id_telephone,type_telephone,numero" +
                    " from telephone order by numero";
            Telephone telephone;
            rs = stmt.executeQuery(strCmd);
            while (rs.next())
            {;
                telephone= new Telephone(rs.getInt(1), rs.getString(2), rs.getString(3));
                liste.add(telephone );
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données ville impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(Telephone objet) {
        return false;
    }

    @Override
    public boolean update(Telephone objet) {
        return false;
    }

    @Override
    public boolean delete(Telephone objet) {
        return false;
    }
}
