package hibernateMetier;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "telephone", schema = "public", catalog = "gites")
public class TelephoneEntity {
    @Id
    @Column(name = "id_telephone")
    private int idTelephone;
    @Basic
    @Column(name = "type_telephone")
    private String typeTelephone;
    @Basic
    @Column(name = "numero")
    private String numero;
    @ManyToMany (mappedBy = "personneTelephones")
    private Set<PersonneEntity> telephonePersonnes = new HashSet<PersonneEntity>();

    public int getIdTelephone() {
        return idTelephone;
    }

    public void setIdTelephone(int idTelephone) {
        this.idTelephone = idTelephone;
    }


    public String getTypeTelephone() {
        return typeTelephone;
    }

    public void setTypeTelephone(String typeTelephone) {
        this.typeTelephone = typeTelephone;
    }


    public String getNumero() {
        return numero;
    }

   public void setNumero(String numero) {
        this.numero = numero;
    }

    Set<PersonneEntity> getTelephonePersonnes(){
        return telephonePersonnes;
    }

    public TelephoneEntity setTelephonePersonnes(Set<PersonneEntity> telephonePersonnes) {
        this.telephonePersonnes = telephonePersonnes;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TelephoneEntity that = (TelephoneEntity) o;

        if (idTelephone != that.idTelephone) return false;
        if (typeTelephone != null ? !typeTelephone.equals(that.typeTelephone) : that.typeTelephone != null)
            return false;
        if (numero != null ? !numero.equals(that.numero) : that.numero != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTelephone;
        result = 31 * result + (typeTelephone != null ? typeTelephone.hashCode() : 0);
        result = 31 * result + (numero != null ? numero.hashCode() : 0);
        return result;
    }
}
