package hibernateMetier;

import javax.persistence.*;

@Entity
@Table(name = "type_equipement_service", schema = "public", catalog = "gites")
public class TypeEquipementServiceEntity {
    private int idTypeService;
    private String libelle;

    @Id
    @Column(name = "id_type_service")
    public int getIdTypeService() {
        return idTypeService;
    }

    public void setIdTypeService(int idTypeService) {
        this.idTypeService = idTypeService;
    }

    @Basic
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

/*@OneToMany (mappedBy = "typeEquipementServiceEntity")
private Set<EquipementEtServiceEntity> equipementsEtServices = new HashSet<EquipementEtServiceEntity>();

    public void addEquipementsEtServices(EquipementEtServiceEntity equipementEtService){
       equipementEtService.setTypeEquipementServiceEntity(this);
       equipementsEtServices.add(equipementEtService);
    }

    public Set<EquipementEtServiceEntity> getEquipementsEtServices() {
        return equipementsEtServices;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeEquipementServiceEntity that = (TypeEquipementServiceEntity) o;

        if (idTypeService != that.idTypeService) return false;
        if (libelle != null ? !libelle.equals(that.libelle) : that.libelle != null) return false;

        return true;
}

    @Override
    public int hashCode() {
        int result = idTypeService;
        result = 31 * result + (libelle != null ? libelle.hashCode() : 0);
        return result;
    }
}
