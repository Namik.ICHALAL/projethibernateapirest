package hibernateMetier;

import javax.persistence.*;

@Entity
@Table(name = "type_periode", schema = "public", catalog = "gites")
public class TypePeriodeEntity {
    private int idTypePeriode;
    private String libellePeriode;

    @Id
    @Column(name = "id_type_periode")
    public int getIdTypePeriode() {
        return idTypePeriode;
    }

    public void setIdTypePeriode(int idTypePeriode) {
        this.idTypePeriode = idTypePeriode;
    }

    @Basic
    @Column(name = "libelle_periode")
    public String getLibellePeriode() {
        return libellePeriode;
    }

    public void setLibellePeriode(String libellePeriode) {
        this.libellePeriode = libellePeriode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypePeriodeEntity that = (TypePeriodeEntity) o;

        if (idTypePeriode != that.idTypePeriode) return false;
        if (libellePeriode != null ? !libellePeriode.equals(that.libellePeriode) : that.libellePeriode != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTypePeriode;
        result = 31 * result + (libellePeriode != null ? libellePeriode.hashCode() : 0);
        return result;
    }
}
