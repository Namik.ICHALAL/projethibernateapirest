package hibernateMetier;


import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "personne", schema = "public", catalog = "gites")
public class PersonneEntity {
    @Id
    @Column(name = "id_personne")
    private int idPersonne;
    @Basic
    @Column(name = "nom_personne")
    private String nomPersonne;
    @Basic
    @Column(name = "prenom_personne")
    private String prenomPersonne;
    @Basic
    @Column(name = "email")
    private String email;
    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "possede", joinColumns = @JoinColumn(name = "id_personne"),
            inverseJoinColumns = @JoinColumn(name = "id_telephone"))
    private Set<TelephoneEntity> personneTelephones= new HashSet<TelephoneEntity>();
    @OneToMany (fetch=FetchType.EAGER)
    @JoinColumn(name = "id_disponibilite")
    private Collection<DisponibiliteEntity> personneDisponibites;


    public int getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(int idPersonne) {
        this.idPersonne = idPersonne;
    }


    public String getNomPersonne() {
        return nomPersonne;
    }

    public void setNomPersonne(String nomPersonne) {
        this.nomPersonne = nomPersonne;
    }


    public String getPrenomPersonne() {
        return prenomPersonne;
    }

    public void setPrenomPersonne(String prenomPersonne) {
        this.prenomPersonne = prenomPersonne;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public PersonneEntity setPersonneTelephones(Set<TelephoneEntity> personneTelephones) {
        this.personneTelephones = personneTelephones;
        return this;
    }

    public Set<TelephoneEntity> getPersonneTelephones() {
        return personneTelephones;
    }

    public Collection<DisponibiliteEntity> getPersonneDisponibites() {
        return personneDisponibites;
    }

    public void setPersonneDisponibites(Collection<DisponibiliteEntity> personneDisponibites) {
        this.personneDisponibites = personneDisponibites;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonneEntity that = (PersonneEntity) o;

        if (idPersonne != that.idPersonne) return false;
        if (nomPersonne != null ? !nomPersonne.equals(that.nomPersonne) : that.nomPersonne != null) return false;
        if (prenomPersonne != null ? !prenomPersonne.equals(that.prenomPersonne) : that.prenomPersonne != null)
            return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPersonne;
        result = 31 * result + (nomPersonne != null ? nomPersonne.hashCode() : 0);
        result = 31 * result + (prenomPersonne != null ? prenomPersonne.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
