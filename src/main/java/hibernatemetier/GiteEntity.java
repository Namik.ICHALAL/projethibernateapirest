package hibernateMetier;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "gite", schema = "public", catalog = "gites")
public class GiteEntity {

    @Id
    @Column(name = "id_gite")
    private Integer idGite;
    @Basic
    @Column(name = "nom_gite")
    private String nomGite;

    @Basic
    @Column(name = "surface_habitable")
    private double surfaceHabitable;
    @Basic
    @Column(name = "nombre_chambres")
    private Integer nombreChambres;
    @Basic
    @Column(name = "nombre_couchage")
    private int nombreCouchage;
    @Basic
    @Column(name = "site_web")
    private String siteWeb;
    @Basic
    @Column(name = "adresse_1")
    private String adresse1;
    @Basic
    @Column(name = "adresse_2")
    private String adresse2;
    @Basic
    @Column(name = "libelle_voie")
    private String libelleVoie;
    @Basic
    @Column(name = "adresse_3")
    private String adresse3;
    @Basic
    @Column(name = "code_postale")
    private String codePostale;
    @ManyToMany (fetch=FetchType.EAGER)
    @JoinTable(name = "avoir", joinColumns = @JoinColumn(name = "id_gite"),
            inverseJoinColumns = @JoinColumn(name = "id_personne"))
    private Set<PersonneEntity> proprietaires = new HashSet<PersonneEntity>();
    @ManyToMany  (fetch=FetchType.EAGER)
    @JoinTable(name = "proposer", joinColumns = @JoinColumn(name = "id_gite"),
            inverseJoinColumns = @JoinColumn(name = "id_service"))
    private Set<EquipementEtServiceEntity> services = new HashSet<EquipementEtServiceEntity>();
    @ManyToMany  (fetch=FetchType.EAGER)
    @JoinTable(name = "est_gere_par", joinColumns = @JoinColumn(name = "id_gite"),
            inverseJoinColumns = @JoinColumn(name = "id_personne"))
    private Set<PersonneEntity> gerants = new HashSet<PersonneEntity>();

    public Set<PersonneEntity> getProprietaires() {
        return proprietaires;
    }

    public GiteEntity setProprietaires(Set<PersonneEntity> proprietaires) {
        this.proprietaires = proprietaires;
        return this;
    }

    public Set<EquipementEtServiceEntity> getServices() {
        return services;
    }

    public GiteEntity setServices(Set<EquipementEtServiceEntity> services) {
        this.services = services;
        return this;
    }

    public Set<PersonneEntity> getGerants() {
        return gerants;
    }

    public GiteEntity setGerants(Set<PersonneEntity> gerants) {
        this.gerants = gerants;
        return this;
    }

    public Integer getIdGite() {
        return idGite;
    }

    public GiteEntity setIdGite(Integer idGite) {
        this.idGite = idGite;
        return this;
    }

    public void setIdGite(int idGite) {
        this.idGite = idGite;
    }


    public String getNomGite() {
        return nomGite;
    }

    public void setNomGite(String nomGite) {
        this.nomGite = nomGite;
    }


    public double getSurfaceHabitable() {
        return surfaceHabitable;
    }

    public void setSurfaceHabitable(double surfaceHabitable) {
        this.surfaceHabitable = surfaceHabitable;
    }


    public Integer getNombreChambres() {
        return nombreChambres;
    }

    public void setNombreChambres(Integer nombreChambres) {
        this.nombreChambres = nombreChambres;
    }


    public int getNombreCouchage() {
        return nombreCouchage;
    }

    public void setNombreCouchage(int nombreCouchage) {
        this.nombreCouchage = nombreCouchage;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }


    public String getAdresse1() {
        return adresse1;
    }

    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }


    public String getAdresse2() {
        return adresse2;
    }

    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }


    public String getLibelleVoie() {
        return libelleVoie;
    }

    public void setLibelleVoie(String libelleVoie) {
        this.libelleVoie = libelleVoie;
    }


    public String getAdresse3() {
        return adresse3;
    }

    public void setAdresse3(String adresse3) {
        this.adresse3 = adresse3;
    }


    public String getCodePostale() {
        return codePostale;
    }


    public void setCodePostale(String codePostale) {
        this.codePostale = codePostale;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GiteEntity that = (GiteEntity) o;

        if (idGite != that.idGite) return false;
        if (Double.compare(that.surfaceHabitable, surfaceHabitable) != 0) return false;
        if (nombreCouchage != that.nombreCouchage) return false;
        if (nomGite != null ? !nomGite.equals(that.nomGite) : that.nomGite != null) return false;
        if (nombreChambres != null ? !nombreChambres.equals(that.nombreChambres) : that.nombreChambres != null)
            return false;
        if (siteWeb != null ? !siteWeb.equals(that.siteWeb) : that.siteWeb != null) return false;
        if (adresse1 != null ? !adresse1.equals(that.adresse1) : that.adresse1 != null) return false;
        if (adresse2 != null ? !adresse2.equals(that.adresse2) : that.adresse2 != null) return false;
        if (libelleVoie != null ? !libelleVoie.equals(that.libelleVoie) : that.libelleVoie != null) return false;
        if (adresse3 != null ? !adresse3.equals(that.adresse3) : that.adresse3 != null) return false;
        if (codePostale != null ? !codePostale.equals(that.codePostale) : that.codePostale != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = idGite;
        result = 31 * result + (nomGite != null ? nomGite.hashCode() : 0);
        temp = Double.doubleToLongBits(surfaceHabitable);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (nombreChambres != null ? nombreChambres.hashCode() : 0);
        result = 31 * result + nombreCouchage;
        result = 31 * result + (siteWeb != null ? siteWeb.hashCode() : 0);
        result = 31 * result + (adresse1 != null ? adresse1.hashCode() : 0);
        result = 31 * result + (adresse2 != null ? adresse2.hashCode() : 0);
        result = 31 * result + (libelleVoie != null ? libelleVoie.hashCode() : 0);
        result = 31 * result + (adresse3 != null ? adresse3.hashCode() : 0);
        result = 31 * result + (codePostale != null ? codePostale.hashCode() : 0);
        return result;
    }
}
