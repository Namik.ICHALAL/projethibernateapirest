package hibernateMetier;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "equipement_et_service", schema = "public", catalog = "gites")
public class EquipementEtServiceEntity {
    @Id
    @Column(name = "id_service")
    private int idService;
    @Basic
    @Column(name = "nom_service")
    private String nomService;



    public int getIdService() {
        return idService;
    }

    public void setIdService(int idService) {
        this.idService = idService;
    }


    public String getNomService() {
        return nomService;
    }

    public void setNomService(String nomService) {
        this.nomService = nomService;
    }



        /*
@ManyToOne
@JoinColumn(name = "id_type_service")
private  TypeEquipementServiceEntity typeEquipementServiceEntity;
    public void setTypeEquipementServiceEntity (TypeEquipementServiceEntity type){
        typeEquipementServiceEntity =type;
    }
    public TypeEquipementServiceEntity getTypeEquipementServiceEntity(){return typeEquipementServiceEntity;}

    @ManyToOne
    @JoinColumn (name = "id_service")
    private EquipementEtServiceEntity equipementEtService ;

    public void setEquipementEtService(EquipementEtServiceEntity equipementEtService) {
        this.equipementEtService = equipementEtService;
    }

    public EquipementEtServiceEntity getEquipementEtService() {
        return equipementEtService;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EquipementEtServiceEntity that = (EquipementEtServiceEntity) o;

        if (idService != that.idService) return false;
        if (nomService != null ? !nomService.equals(that.nomService) : that.nomService != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idService;
        result = 31 * result + (nomService != null ? nomService.hashCode() : 0);
        return result;
    }
}
