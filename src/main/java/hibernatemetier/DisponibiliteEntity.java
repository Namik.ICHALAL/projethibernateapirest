package hibernateMetier;

import javax.persistence.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "disponibilite", schema = "public", catalog = "gites")
public class DisponibiliteEntity {
    @Id
    @Column(name = "id_disponibilite")
    private int idDisponibilite;
    @Basic
    @Column(name = "jour")
    private String jour;
    @Basic
    @Column(name = "heure_debut")
    private Time heureDebut;
    @Basic
    @Column(name = "heure_fin")
    private Time heureFin;



    public int getIdDisponibilite() {
        return idDisponibilite;
    }

    public void setIdDisponibilite(int idDisponibilite) {
        this.idDisponibilite = idDisponibilite;
    }


    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }


    public Time getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(Time heureDebut) {
        this.heureDebut = heureDebut;
    }


    public Time getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(Time heureFin) {
        this.heureFin = heureFin;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisponibiliteEntity that = (DisponibiliteEntity) o;

        if (idDisponibilite != that.idDisponibilite) return false;
        if (jour != null ? !jour.equals(that.jour) : that.jour != null) return false;
        if (heureDebut != null ? !heureDebut.equals(that.heureDebut) : that.heureDebut != null) return false;
        if (heureFin != null ? !heureFin.equals(that.heureFin) : that.heureFin != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDisponibilite;
        result = 31 * result + (jour != null ? jour.hashCode() : 0);
        result = 31 * result + (heureDebut != null ? heureDebut.hashCode() : 0);
        result = 31 * result + (heureFin != null ? heureFin.hashCode() : 0);
        return result;
    }
}
